package easykafka

import "github.com/Shopify/sarama"

//MsgData 数据
type MsgData struct {
	//实际的消息数据
	msg *sarama.ConsumerMessage
	//消费者组会话
	session sarama.ConsumerGroupSession
}

func (d *MsgData) Data() []byte {
	return d.msg.Value
}

func (d *MsgData) Topic() string {
	return d.msg.Topic
}

func (d *MsgData) Partition() int32 {
	return d.msg.Partition
}

func (d *MsgData) Offset() int64 {
	return d.msg.Offset
}
