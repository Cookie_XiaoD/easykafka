package spec

//SASLInfo 简单验证与安全层信息 用于生产者与消费者同Broker之间的安全验证
type SASLInfo struct {
	//用户名
	UserName string
	//密码（明文）
	Password string
}
