package spec

//Msg 表示一个Kafka的数据消息
type Msg interface {
	//当前数据所属Topic
	Topic() string
	//当前数据所属Topic下的分区
	Partition() int32
	//当前数据的分区偏移量
	Offset() int64
	//实际数据内容
	Data() []byte
}
