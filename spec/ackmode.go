package spec

//AckMode 表示生产者生产消息时的确认模式
//该模式关系到数据是否丢失
type AckMode int16

const (
	// 不等待broker同步完成的确认 该模式可靠性最低但性能最高
	WaitNone AckMode = 0
	// 等待leader成功收到数据并确认 若leader在数据还未复制到follower前就崩溃则丢失消息
	WaitLeader AckMode = 1
	// 在follower副本确认接收到数据后才算发送完成 具体需多少个follower确认取决于集群的min.insync.replicas配置项
	WaitAll AckMode = -1
)
