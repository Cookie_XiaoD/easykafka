package spec

import "context"

//Producer 消息生产者
type Producer interface {
	//同步生产消息
	SyncProduce(topic, key string, data interface{}) (n int, err error)

	//异步生产消息
	AsyncProduce(ctx context.Context, topic, key string, data interface{}) (n int, err error)

	//Close 关闭生产者
	Close() (err error)
}
