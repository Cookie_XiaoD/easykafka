package spec

import "context"

//Consumer 消息消费者
type Consumer interface {
	//Confirm 确认单笔消息及该消息之前的所有消息
	Confirm(msg Msg) error
	//ConfirmBatch 批量确认不同Topic与Partition的最后一笔消息
	ConfirmBatch(msgs []Msg) error
	//StartBlock 开始进行阻塞消费
	StartBlock(ctx context.Context)
	//Close 关闭消费者
	Close() (err error)
}
