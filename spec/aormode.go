package spec

//AORMode(auto.offset.reset) Kafka的自动偏移量重置方式
//同消费者组的消费者无初始偏移或当前偏移在服务器上不存在时消费者的消费行为
type AORMode int16

const (
	//从消费开始后产生的新消息开始消费
	Newest AORMode = 0
	//从最早的消息开始消费
	Earliest AORMode = 1
)
